defmodule Proto.MPS7Worker do
  @moduledoc false
  use GenServer

  require Logger
  alias Proto.{Calculator, MPS7}

  def start_link(file: file) do
    GenServer.start_link(__MODULE__, file)
  end

  def init(file) when is_binary(file) do
    {:ok, %{}, {:continue, file}}
  end

  def handle_continue(file, state) do
    {{type, version, expected_records}, records} = get_records(file)
    [f | _] = records
    Logger.info("Log Type: #{type}")
    Logger.info("Version: #{inspect(version)}")
    Logger.info("Expected Records: #{expected_records}")
    Logger.info("Parsed Records: #{length(records)}")
    Logger.info("First: #{inspect(f)}")
    credits = Calculator.amount(:credit, records)
    Logger.info("total credit amount=#{credits}")
    debits = Calculator.amount(:debit, records)
    Logger.info("total debit amount=#{debits}")
    autopay_starts = Calculator.num_entries(:start_autopay, records)
    Logger.info("autopays started=#{autopay_starts}")
    autopay_ends = Calculator.num_entries(:end_autopay, records)
    Logger.info("autopays ended=#{autopay_ends}")
    user_balance = Calculator.balance_for(2_456_938_384_156_277_127, records)
    Logger.info("balance for user 2456938384156277127=#{user_balance}")
    user_balance1 = Calculator.balance_for(5_600_924_393_587_988_459, records)
    Logger.info("balance for user 5600924393587988459=#{user_balance1}")
    {:noreply, state}
  end

  def get_records(file) do
    MPS7.parse(file)
  end
end

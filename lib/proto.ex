defmodule Proto do
  @moduledoc """
  Documentation for Proto.
  """
  use Application
  require Logger

  alias Proto.MPS7Worker

  def start(_, _) do
    log_file = Path.join([:code.priv_dir(:proto), "txnlog.dat"])

    children = [
      {MPS7Worker, [file: log_file]}
    ]

    opts = [strategy: :one_for_one, name: Proto.Supervisor]
    Supervisor.start_link(children, opts)
  end
end

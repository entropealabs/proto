defmodule Proto.MPS7 do
  def parse(file) do
    file
    |> open()
    |> header()
    |> records()
  end

  def open(file) do
    File.read!(file)
  end

  def header(<<"MPS7", version::binary-1, num_records::integer-32, records::binary()>>) do
    {{"MPS7", version, num_records}, records}
  end

  def records({header, records}) do
    {header, parse_records(records, [])}
  end

  def parse_records(<<>>, acc), do: acc

  def parse_records(
        <<0, timestamp::32, user_id::64, amount::float-size(64), rest::binary()>>,
        acc
      ) do
    parse_records(rest, [{:debit, timestamp, user_id, amount} | acc])
  end

  def parse_records(
        <<1, timestamp::32, user_id::64, amount::float-size(64), rest::binary()>>,
        acc
      ) do
    parse_records(rest, [{:credit, timestamp, user_id, amount} | acc])
  end

  def parse_records(
        <<2, timestamp::32, user_id::64, rest::binary()>>,
        acc
      ) do
    parse_records(rest, [{:start_autopay, timestamp, user_id} | acc])
  end

  def parse_records(
        <<3, timestamp::32, user_id::64, rest::binary()>>,
        acc
      ) do
    parse_records(rest, [{:end_autopay, timestamp, user_id} | acc])
  end
end

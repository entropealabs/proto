defmodule Proto.Calculator do
  @moduledoc false

  def amount(type, records) do
    Enum.reduce(records, 0, fn
      {^type, _, _, amnt}, acc -> acc + amnt
      _, acc -> acc
    end)
  end

  def num_entries(type, records) do
    Enum.count(records, fn
      {^type, _, _, _} -> true
      {^type, _, _} -> true
      _ -> false
    end)
  end

  def balance_for(user_id, records) do
    Enum.reduce(records, 0, fn
      {:credit, _, ^user_id, amnt}, acc -> acc + amnt
      {:debit, _, ^user_id, amnt}, acc -> acc - amnt
      _, acc -> acc
    end)
  end
end

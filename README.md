# Proto

A quick exercise from https://homework.adhoc.team/proto/

An Elixir application to parse and calculate the MPS7 transaction log format

There are no dependencies other than Elixir and Erlang.

The recommended way to run this is with docker-compose.

From the root project directory run

$ docker-compose up

A .tool-versions file is also included for the asdf-vm plugin system. If you prefer to install the required programs directly on your machine, from the project directory, you can run

$ asdf install


This will install docker-compose, elixir and erlang... be warned erlang takes a while to compile. It's mush faster to just run "docker-compose up"

There's some additional output outside of what was required. I found an additional record, and the user id provided had zero balance. So there's some additional information for a sanity check.
